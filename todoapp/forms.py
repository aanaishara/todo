from django import forms
from .models import *
from django.contrib.auth.models import User


class TodoForm(forms.ModelForm):
	class Meta:
		model=Comment
		fields=[ "text"]

class TodoCreateForm(forms.ModelForm):
	class Meta:
		model=Todolist
		fields="__all__"

class SigninForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your username...'
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter your password...'
    }))


class SignupForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput())
	email=forms.CharField(widget=forms.EmailInput())
	password=forms.CharField(widget=forms.PasswordInput())
	confirm_password=forms.CharField(widget=forms.PasswordInput())

	def clean_username(self):
		uname = self.cleaned_data["username"]
		if User.objects.filter(username=uname).exists():
			raise forms.ValidationError("username already exists")
		return uname



	def clean_confirm_password(self):
		password = self.cleaned_data["password"]
		c_pword = self.cleaned_data["confirm_password"]
		if password != c_pword :
			raise forms.ValidationError("password didn't match ")
		return c_pword