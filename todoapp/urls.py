from django.urls import path
from .views import *
app_name = "todoapp"
urlpatterns = [
    path("", SigninView.as_view(), name="signin"),
    path("signup/",SignupView.as_view(),name="signup"),
    path("signout/",SignoutView.as_view(),name="signout"),
    path("home/", HomeView.as_view(), name="home"),
    path('todolist/', TodoListView.as_view(), name='todolist'),
    path("todolist/<int:pk>/tododetail/",
         TodoDetailView.as_view(), name="todolistdetail"),
    path("todolistdetail/<int:pk>/step/",StepCreateView.as_view(),name="stepcreate"),
    path('todolist/create/',
     TodoCreateView.as_view(), name="todocreate"),
    path("ongoing/todolist/",OngoingView.as_view(),name="ongoingtodolist"),
    path("expired/todolist/",ExpireView.as_view(),name="expiretodolist"),


 
]