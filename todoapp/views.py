from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.conf import settings
from django.core.mail import send_mail
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views.generic import *
from .forms import *
from django.contrib.auth.models import User
from .models import *
from datetime import datetime, date, timedelta


# Create your views here.

today = date.today()

class SigninView(FormView):
    template_name = "signin.html"
    form_class = SigninForm
    success_url = reverse_lazy('todoapp:home')

    def form_valid(self, form):
        username = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        # print(username,password,"welcome")

        user = authenticate(username=username, password=password)
        # print(user,"welcome")
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, "signin.html",
                          {"error": "invalid username or password",
                           "form": form

                           })

        return super().form_valid(form)

class SignupView(FormView):
    template_name="signup.html"
    form_class=SignupForm
    success_url="/"
    def form_valid(self,form):
        uname=form.cleaned_data["username"]
        email=form.cleaned_data["email"]
        pword=form.cleaned_data["password"]
        
        User.objects.create_user(uname,email,pword)

        return super().form_valid(form)

class SignoutView(View):
    def get (self,request):
        logout(request)
        return redirect(reverse_lazy('todoapp:signin'))

class BaseMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["todoform"] = TodoForm


        return context
class HomeView(LoginRequiredMixin,BaseMixin,TemplateView):
    template_name = "home.html"
    def dispatch(self,request,*args,**kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("/signin")
        return super().dispatch(request,*args,**kwargs)


    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)
        context['today'] = today
        context['todolist']=Todolist.objects.filter(end_date__gt=today).order_by('-start_date')
        context['exptodolist'] = Todolist.objects.filter(end_date__lt=today).order_by('-start_date')

        
      
        return context



class TodoListView(LoginRequiredMixin,ListView):
    template_name = "todolist.html"
    queryset = Todolist.objects.filter().order_by('-start_date')
    context_object_name = "todolist"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = today
        context['exptodolist'] = Todolist.objects.filter(end_date__lt=today)
        print(today)
        return context

class OngoingView(LoginRequiredMixin,ListView):
    template_name = "ongointodolist.html"
    queryset = Todolist.objects.filter().order_by('-start_date')
    context_object_name = "ongoingtodolist"


class ExpireView(LoginRequiredMixin,ListView):
    template_name = "expiretodolist.html"
    queryset = Todolist.objects.filter().order_by('-start_date')
    context_object_name = "todolist"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = today
        context['exptodolist'] = Todolist.objects.filter(end_date__lt=today)
        print(today)
        return context


class TodoDetailView(LoginRequiredMixin,BaseMixin,DetailView):
    template_name = "todolistdetail.html"
    model = Todolist
    context_object_name = "todoobject"

class StepCreateView(LoginRequiredMixin,BaseMixin,CreateView):
    template_name="todo.html"
    form_class=TodoForm
    success_url=reverse_lazy('todoapp:todolist')
    def form_valid (self,form):
        todolist_id=self.kwargs["pk"]
        todolist=Todolist.objects.get(id=todolist_id)
        form.instance.todolist=todolist
   

class TodoCreateView(LoginRequiredMixin,BaseMixin, CreateView):
    template_name = "todocreate.html"
    form_class = TodoCreateForm
    success_url = reverse_lazy('todoapp:home')
