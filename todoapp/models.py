from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Todolist(TimeStamp):
    title = models.CharField(max_length=300)
    content = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    # author =models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Comment(TimeStamp):
    todolist = models.ForeignKey(Todolist, on_delete=models.CASCADE)
    text=models.TextField()
